# _*_ coding: utf-8 _*_
__author__ = 'HaoChenXiao'
from django.conf.urls import url, include
from dbmonitor import views

urlpatterns = [
	url(r'^mysql_dblist/$', views.MySQLListView.as_view(), name = 'mysql_dblist'),
	url(r'^mysqldb_edit/$', views.MySQLDBEditView.as_view(), name = 'mysqldb_edit'),
	url(r'^mysqldb_start/$', views.MySQLDBStartView.as_view(), name = 'mysqldb_start'),
	url(r'^mysqldb_stop/$', views.MySQLDBStopView.as_view(), name = 'mysqldb_stop'),
	url(r'^mysqldb_delete/$', views.MySQLDBDeleteView.as_view(), name = 'mysqldb_delete'),
	url(r'^mysql_detail/$', views.MySQLDBDetailView.as_view(), name = 'mysql_detail'),
	url(r'^mysql_slowlog/$', views.MySQLSlowLogView.as_view(), name = 'mysql_slowlog'),
	url(r'^mysql_slowlog_detail/$', views.MySQLSlowLogDetailView.as_view(), name = 'mysql_slowlog_detail'),



]