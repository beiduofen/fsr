# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django.views.generic import TemplateView, ListView, DetailView, View
from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from confs.views import get_pagerange
from resources.models import ServerUser, NewServer


# 主机资源用户展示
class ServerUserListView(LoginRequiredMixin, ListView):
    
    template_name = 'serveruser/serveruser_list.html'
    model = ServerUser
    paginate_by = 10
    ordering = 'id'

    def get_context_data(self, **kwargs):
        context = super(ServerUserListView, self).get_context_data(**kwargs)
        context['page_range'] = get_pagerange(context['page_obj'])
        return context


# 主机资源用户创建
class ServerUserCreateView(LoginRequiredMixin, TemplateView):
    template_name = 'serveruser/serveruser_create.html'

    def get_context_data(self, **kwargs):
        context = super(ServerUserCreateView, self).get_context_data(**kwargs)
        #context['server_list'] = NewServer.objects.filter(server_user=None)
        context['server_list'] = NewServer.objects.all()
        return context

    def post(self, request):
        ret = {'status': 0}
        try:
            server_user = ServerUser.objects.create(
                name=request.POST.get('name'),
                username=request.POST.get('username'),
                password=request.POST.get('password'),
                privatekey=request.POST.get('privatekey'),
                info=request.POST.get('info'),
            )
            serveruser_list = request.POST.getlist('serveruser_id')
            for server_id in serveruser_list:
                server = NewServer.objects.get(id=server_id)
                server.server_user = server_user
                server.save()
            ret['msg'] = '创建成功'
        except Exception as e:
            print(e)
            ret['status'] = 1
            ret['msg'] = str(e)

        return JsonResponse(ret)


# 主机资源用户详情信息
class ServerUserDetailView(LoginRequiredMixin, DetailView):
    template_name = 'serveruser/serveruser_detail.html'
    model = ServerUser

    def get_context_data(self, **kwargs):
        context = super(ServerUserDetailView, self).get_context_data(**kwargs)
        context['server_list'] = NewServer.objects.all()
        return context


# 主机资源用户修改
class ServerUserModifyView(LoginRequiredMixin, View):

    def post(self, request):
        ret = {'status': 0}
        try:
            serveruser_id = request.POST.get('serveruser_id')
            password = request.POST.get('password')
            privatekey = request.POST.get('privatekey')
            server_id_list = request.POST.getlist('server_id')
            print (server_id_list,password,privatekey)
            if password and privatekey:
                ServerUser.objects.filter(pk=serveruser_id).update(
                    name=request.POST.get('name'),
                    username=request.POST.get('username'),
                    password=password,
                    privatekey = privatekey,
                    info=request.POST.get('info'),
                )
            elif password:
                ServerUser.objects.filter(pk=serveruser_id).update(
                    name=request.POST.get('name'),
                    username=request.POST.get('username'),
                    password=password,
                    info=request.POST.get('info'),
                )
            elif privatekey:
                ServerUser.objects.filter(pk=serveruser_id).update(
                    name=request.POST.get('name'),
                    username=request.POST.get('username'),
                    privatekey=privatekey,
                    info=request.POST.get('info'),
                )
            else:
                ServerUser.objects.filter(pk=serveruser_id).update(
                    name=request.POST.get('name'),
                    username=request.POST.get('username'),
                    info=request.POST.get('info'),
                )



            serveruser = ServerUser.objects.get(pk=serveruser_id)
            serveruser.newserver_set.clear()
            for server_id in server_id_list:
                server = NewServer.objects.get(pk=server_id)
                server.server_user_id = serveruser_id
                server.save()
            ret['msg'] = '修改成功'
        except Exception as e:
            ret['status'] = 1
            ret['msg'] = str(e)
        return JsonResponse(ret)

class ServerUserDeleteView(LoginRequiredMixin, View):

    def post(self, request):
        ret = {'status': 0}
        try:
            serveruser_id = request.POST.get('serveruser_id')
            serveruser = ServerUser.objects.get(id=serveruser_id)
            serveruser.delete()
            ret['msg'] = '资产用户删除成功'
        except Exception as e:
            ret['status'] = 1
            ret['msg'] = str(e)
        return JsonResponse(ret)